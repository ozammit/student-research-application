# Students Research Plugin #

## About ##

This application is part of my research where my main aim is to help students study and find relevant information online. The following project was published in the [Artificial Intelligence XXXVI](https://link.springer.com/book/10.1007/978-3-030-34885-4) conference, paper [Exposing Knowledge: Providing a Real-Time View of the Domain Under Study for Students](https://link.springer.com/chapter/10.1007%2F978-3-030-34885-4_9). This application will provide a full visibility of what you are searching for and will also suggest keywords that you can use during your research. The more you use the application and the more you click the suggestions provided, the more accurate future predictions will be.

Please feel free to contact me if you have any queries regarding the application or [my research](https://www.researchgate.net/profile/Omar_Zammit). You can also contact me by email on [ozammit@iee.org](ozammit@iee.org).

To clone this project use:

~~~
git clone https://ozammit@bitbucket.org/ozammit/student-research-application.git
~~~

## Installation ##

The application is made up of two components, a background server that runs in a console application and a Google Chrome Extension. The following is the recommended setup.

**Install Background Server**

- Download file *install_file.exe*. And double click the file to install.
- If you have *User Account Control* switched on, Windows will ask to allow the application to run. Click **Yes**.
- Select where the application will be installed. The default path is *C:\Program Files (x86)\Students Research Plugin* but you can install the application in any folder. Click **Next**.
- Select if you want a desktop shortcut and display the *Read Me* file at the end of the installation and click **Next**. It is recommended to create a desktop shortcut for easy access the background server.
- Review the details and click **Install**.
- Since the installation has a *Python interpreter* and all the required libraries it might take some time.
- Once done click *Finish*, the wizard will close and the background console will open.
- Wait until the following line appears in the console:

~~~
2020-06-18 16:06:12.011796:[foundation.services.http_server] INFO: HTTP server started on port 8080
~~~

- This line indicates that the background server is running and listening on port 8080. While the Google Chrome Extension is being used, the background server will display the activities being done.
- A folder will be created in your user's folder called ***research_data***. Do not delete this folder since it contains all the data collected by the background server.

**Important notes**

- In order for the Google Chrome Extension to work, the background server console should be running.
- Close the background server to switch it off.

**Install the Google Chrome Extension**

The Google Chrome Extension source files are located in your installation folder. Unless modified its in *C:\Program Files (x86)\Students Research Plugin*. In order to install it :

- Open a Google Chrome Browser and navigate to *chrome://extensions*
- Ensure that **Developer mode** located on top right of the browser is switched on.
- Click **Load unpacked** button.
- Navigate to your installation folder and select the folder *plugin*.
- A small icon should appear in the browser. Click this icon to launch the user extension user interface.
- The following image shows the user interface displaying some results.

**NOTE**: The browser may complain that you have Developer mode on, just ignore it. We opted not to package the extension because we are encouraging front end gurus to contribute in improving the appearance of our extension.

**About the Google Chrome Extension**

![Plugin](images/plugin.png)

1. Last Searched Keywords: Last keywords searched by the student.
2. Similar Searched Keywords: Using similarity analysis as suggested by this contains a list of similar keywords previously searched by the student.
3. Word Cloud : This contains keyphrases related to the domain. This allows students to be exposed to new terminologies and potential search keyphrases.
4. Generic Feedback : Allows students to share if they are finding relevant information.

## Troubleshooting ##
**I want my 'research_data' folder to be located somewhere else**

If you want that the applications stores the data somewhere else rather than your user folder, what you need to do is modify the *settings.yaml* file. The file is located in the installation folder, unless modified this should be *C:\Program Files (x86)\Students Research Plugin*. Locate the entry below:

~~~
# Data folder where the data will be stored
DataFolder: <HOME>/app_data/local_server/data/
~~~

Change the part *\<HOME>/app_data/local_server/data/* with your desired location. It is very important not to change other contents within this file because this determines the behaviour of the application. It is recommended that you take a backup before modifying this file.   



**I want to learn more about my searches, how can I request a report?**

We have scripts and applications that can generate reporting based on your collected data. For the time being we are working to making these public so they are not available. However if you contact me by email I will manually generate the reports and send them to you by email.
